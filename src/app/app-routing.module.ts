import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'view/:name', component: ViewComponent },
  { path: '**', component: ListComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
