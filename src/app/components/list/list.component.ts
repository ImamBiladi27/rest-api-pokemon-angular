import { Component, OnInit, OnDestroy } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { concat, Subscription } from 'rxjs';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  loading: boolean = false;

  constructor(private pokemonService: PokemonService) { }

  get pokemons(): any[]{
    return this.pokemonService.pokemons;
}

   set subscription(subscription: Subscription) {
    this.subscriptions.push(subscription);
   }
  
  ngOnInit(): void {
    if (!this.pokemons.length) {
      this.loadMore();
  }
  }

  ngOnDestroy(): void {
    
  this.subscriptions.forEach(subscription => subscription ? subscription.unsubscribe() : 0);
  }

  loadMore(): void {
    this.loading = true;
    this.subscription = this.pokemonService.getNext().subscribe((response: { next: string; results: any[]; }) => {
      this.pokemonService.next = response.next;
      const details = response.results.map((i: any) => this.pokemonService.get(i.name));
      this.subscription = concat(...details).subscribe((response: any) => {
        this.pokemonService.pokemons.push(response);
      });
    }, (error: any) => console.log('Error Occurred:', error), () => this.loading = false);
  }

  
  getType(pokemon: any): string {
    return this.pokemonService.getType(pokemon);
  }

}
